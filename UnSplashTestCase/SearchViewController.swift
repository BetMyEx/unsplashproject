//
//  SearchViewController.swift
//  UnSplashTestCase
//
//  Created by Admin on 27.06.2022.
//

import UIKit

class SearchViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    var searchPhotos: [Photo] = []
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 30
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.identifier, for: indexPath) as! CollectionViewCell
        cell.collectionCellImageView.downloaded(from: URL(string: (self.searchPhotos[indexPath.row]?.urls?.small)!)!)
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .blue
    }
    
}
