import UIKit
import Alamofire

let accessKey = "gooRZR2nhaRw3rOYWzkxVJFWEHP7f0Z5-sTp7GLgHP0"


struct GetRandomPhotosRequest: Encodable {
    let client_id: String
    let count: Int
    let orientation: String
}
struct GetSearchResults: Encodable {
    let client_id: String
    let count: Int
    let orientation: String
    let query: String
    let per_page: Int
}


class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UISearchControllerDelegate, UISearchBarDelegate {
    
    var photosArray: [Photo?] = []
    private var collectionView: UICollectionView?
    private var refresher: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Random Collection"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.view.backgroundColor = .gray
        customTabBarSetting(title: "Collection", systemImage: "folder.fill", tag: 0)
        collectionViewSetting()
        refreshSetting()
        getRandomPhotos()
    }
    
    
    
    
    //MARK: - Search Controller
    func searchControllerSetting() {
        let searchController = UISearchController()
        navigationItem.searchController = searchController
        searchController.delegate = self
        searchController.searchBar.delegate = self
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("Searching")
        searchBar.resignFirstResponder()
        if searchBar.text != nil {
            searchPhoto(search: searchBar.text!)
        }
    }
    //MARK: - Custom tabBarItem
    func customTabBarSetting(title: String, systemImage: String, tag: Int) {
        let mainTabBarItem = UITabBarItem(title: title, image: UIImage(systemName: systemImage), tag: tag)
        self.tabBarItem = mainTabBarItem
    }
    
    //MARK: - CollectionView setting
    func collectionViewSetting() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: view.frame.size.width / 2 - 4,
                                 height: view.frame.size.width / 3 - 4)
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 1
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        guard let collectionView = collectionView else { return }
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(CollectionViewCell.self, forCellWithReuseIdentifier: CollectionViewCell.identifier)
        collectionView.frame = view.bounds
        collectionView.alwaysBounceVertical = true
        view.addSubview(collectionView)
        searchControllerSetting()
    }
    
    func refreshSetting() {
        self.refresher = UIRefreshControl()
        self.refresher.tintColor = UIColor.red
        self.refresher.backgroundColor = .clear
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.collectionView?.refreshControl = refresher
    }
    
    @objc func loadData() {
        self.collectionView!.refreshControl?.beginRefreshing()
        getRandomPhotos()
        stopRefresher()
    }
    
    func stopRefresher() {
        self.refresher.endRefreshing()
    }
    
    //MARK: - CollectionView DataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photosArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.identifier, for: indexPath) as! CollectionViewCell
        switch self.photosArray.isEmpty {
        case true:
            cell.collectionCellImageView.image = UIImage(named: "")
        default:
            cell.collectionCellImageView.downloaded(from: URL(string: (self.photosArray[indexPath.row]?.urls?.small)!)!)
        }
        
        return cell
    }
    
    
    //MARK: - Move to detail
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailViewController = DetailViewController()
        guard photosArray.isEmpty != true else { return }
        detailViewController.photo = photosArray[indexPath.row]
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    //MARK: - Network Request
    func getRandomPhotos() {
        let url = "https://api.unsplash.com/photos/random"
        let params = GetRandomPhotosRequest(client_id: accessKey, count: 30, orientation: "landscape")
        let headers: HTTPHeaders = [
            "Accept-Version": "v1",
            "Accept": "application/json"
        ]
        AF.request(url, method: .get, parameters: params, encoder: URLEncodedFormParameterEncoder.default, headers: headers, interceptor: nil).responseDecodable(of: [Photo].self) { [self] response in
            switch(response.result) {
            case .success:
                if let photos: [Photo] = response.value {
                    var bufferArray = [Photo]()
                    photos.forEach { photo in
                        bufferArray.append(photo)
                    }
                    guard bufferArray.count != 0 else {
                        emptySearchResult()
                        return
                    }
                    photosArray = bufferArray
                    collectionView?.reloadData()
                }
            case let .failure(error):
                
                print(error)
            }
        }
    }
    //MARK: - Requst for searching photos
    func searchPhoto(search: String) {
        let url = "https://api.unsplash.com/search/photos"
        let params = GetSearchResults(client_id: accessKey, count: 30, orientation: "landscape", query: search, per_page: 30)
        let headers: HTTPHeaders = [
            "Accept-Version": "v1",
            "Accept": "application/json"
        ]
        AF.request(url, method: .get, parameters: params, encoder: URLEncodedFormParameterEncoder.default, headers: headers, interceptor: nil).responseDecodable(of: Results.self) { [self] response in
            switch(response.result) {
            case .success:
                if let results: Results = response.value {
                    var bufferArray = [Photo]()
                    results.results!.forEach { photo in
                        bufferArray.append(photo)
                    }
                    guard bufferArray.count != 0 else {
                        emptySearchResult()
                        return
                    }
                    photosArray = bufferArray
                    collectionView?.reloadData()
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    //MARK: - Empty search result

    func emptySearchResult() {
        let alertEmptySearchResult = UIAlertController(title: "No exact matches", message: "Please try another request", preferredStyle: UIAlertController.Style.alert)
        
        alertEmptySearchResult.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(alertEmptySearchResult, animated: true, completion: nil)
    }
}
//MARK: - Download and set image from URL
extension UIImageView {
    func downloaded(from url: URL, contentMode mode: ContentMode = .scaleAspectFill) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
            else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: ContentMode = .scaleAspectFill) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
    
}





