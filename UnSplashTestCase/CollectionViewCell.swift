import UIKit

class CollectionViewCell: UICollectionViewCell {
    static let identifier = "CustomCollectionViewCell"
    
    let collectionCellImageView: UIImageView = {
        let cellImageView = UIImageView()
        cellImageView.layer.cornerRadius = 14
        cellImageView.backgroundColor = .gray
        cellImageView.clipsToBounds = true
        cellImageView.translatesAutoresizingMaskIntoConstraints = false
        cellImageView.contentMode = .scaleAspectFill
        return cellImageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(collectionCellImageView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        collectionCellImageView.frame = self.bounds
    }
    public func configure(image: UIImage ) {
        collectionCellImageView.image = image
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        collectionCellImageView.image = nil
    }
}
