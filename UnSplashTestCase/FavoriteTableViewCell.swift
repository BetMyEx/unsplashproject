import UIKit
import MarqueeLabel

class FavoriteTableViewCell: UITableViewCell {
    static let identifier = "FavoriteTableVIewCell"
    
    let cellView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 14
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    let collectionCellImageView: UIImageView = {
        let cellImageView = UIImageView()
        cellImageView.layer.cornerRadius = 14
        cellImageView.backgroundColor = .gray
        cellImageView.clipsToBounds = true
        cellImageView.translatesAutoresizingMaskIntoConstraints = false
        cellImageView.contentMode = .scaleAspectFill
        return cellImageView
    }()
    
    let collectionCellNameLabel: MarqueeLabel = {
        let label = MarqueeLabel()
        label.text = "Test"
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let dateOfCreationLabel: UILabel = {
        let label = UILabel()
        label.text = "Date"
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let locationLabel: MarqueeLabel = {
        let label = MarqueeLabel()
        label.text = "Location"
        label.textAlignment = .right
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let downloadslabel: UILabel = {
        let label = UILabel()
        label.text = "Downloads"
        label.textAlignment = .right
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        collectionCellImageView.frame = self.bounds
        addSubview(cellView)
        NSLayoutConstraint.activate([
            cellView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            cellView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            cellView.leadingAnchor.constraint(equalTo: leadingAnchor),
            cellView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0)
        ])

        cellView.addSubview(collectionCellImageView)
        NSLayoutConstraint.activate([
            collectionCellImageView.topAnchor.constraint(equalTo: cellView.topAnchor, constant: 4),
            collectionCellImageView.bottomAnchor.constraint(equalTo: cellView.bottomAnchor, constant: -4),
            collectionCellImageView.leadingAnchor.constraint(equalTo: cellView.leadingAnchor, constant: 4),
            collectionCellImageView.widthAnchor.constraint(equalTo: collectionCellImageView.heightAnchor)
        ])
        

        cellView.addSubview(collectionCellNameLabel)
        NSLayoutConstraint.activate([
            collectionCellNameLabel.leadingAnchor.constraint(equalTo: collectionCellImageView.trailingAnchor, constant: 8),
            collectionCellNameLabel.topAnchor.constraint(equalTo: cellView.topAnchor, constant: 4),
            collectionCellNameLabel.widthAnchor.constraint(greaterThanOrEqualToConstant: 64)
        ])
        

        cellView.addSubview(dateOfCreationLabel)
        NSLayoutConstraint.activate([
            dateOfCreationLabel.bottomAnchor.constraint(equalTo: cellView.bottomAnchor, constant: -4),
            dateOfCreationLabel.leadingAnchor.constraint(equalTo: collectionCellImageView.trailingAnchor, constant: 8)
        ])

        
        cellView.addSubview(locationLabel)
        NSLayoutConstraint.activate([
        locationLabel.trailingAnchor.constraint(equalTo: cellView.trailingAnchor, constant: -4),
        locationLabel.topAnchor.constraint(equalTo: cellView.topAnchor, constant: 4),
        locationLabel.leadingAnchor.constraint(equalTo: collectionCellNameLabel.trailingAnchor, constant: 4)
        ])
        
        cellView.addSubview(downloadslabel)
        NSLayoutConstraint.activate([
        downloadslabel.bottomAnchor.constraint(equalTo: cellView.bottomAnchor, constant: -4),
        downloadslabel.trailingAnchor.constraint(equalTo: cellView.trailingAnchor, constant: -4)
        ])
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


