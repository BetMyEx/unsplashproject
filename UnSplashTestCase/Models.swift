import Foundation
struct Results: Codable {
    let results: [Photo]?
}
struct Photo: Codable, Equatable {
    static func == (lhs: Photo, rhs: Photo) -> Bool {
        lhs.urls?.small == rhs.urls?.small
    }
    let downloads: Int?
    let created_at: String?
    let likes: Int?
    let user: User?
    let urls: Urls?
    let location: Location?
    
}

struct Location: Codable {
    let name: String?
}

struct User: Codable {
    let name: String?
}

struct Urls: Codable {
    let small: String?
    let regular: String?
}

