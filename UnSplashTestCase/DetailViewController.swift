import UIKit
import MarqueeLabel

class DetailViewController: UIViewController {
    
    var photo: Photo?
    private var imageView = UIImageView()
    private var nameLabel = UILabel()
    private var dateLabel = UILabel()
    private var locationLabel = MarqueeLabel()
    private var downloadsLabel = UILabel()
    private var addToFavoritesButton = UIButton(type: .roundedRect)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        imageViewSettings()
        nameLabelSetting()
        locationLabelSetting()
        dateLabelSetting()
        addToFavoritesButtonSetting()
        
    }
    
    func imageViewSettings() {
        self.view.addSubview(imageView)
        imageView.layer.cornerRadius = 10
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.downloaded(from: (photo?.urls?.regular)!)
        NSLayoutConstraint.activate([
            self.imageView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 5),
            self.imageView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -5),
            self.imageView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 20),
            self.imageView.heightAnchor.constraint(equalToConstant: 250)
        ])
    }
    
    func nameLabelSetting() {
        self.view.addSubview(nameLabel)
        nameLabel.text = ("Created by: \(photo?.user?.name ?? "no name information")")
        nameLabel.textColor = .black
        nameLabel.contentMode = .scaleAspectFill
        nameLabel.font = .systemFont(ofSize: 20)
        nameLabel.textAlignment = .center
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.nameLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            self.nameLabel.topAnchor.constraint(equalTo: self.imageView.bottomAnchor, constant: 10),
            self.nameLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 5),
            self.nameLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -5),
            self.nameLabel.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    func locationLabelSetting() {
        self.view.addSubview(locationLabel)
        locationLabel.text = ("Location: \(photo?.location?.name ?? "no location information") ")
        locationLabel.textColor = .black
        locationLabel.contentMode = .scaleAspectFill
        locationLabel.font = .systemFont(ofSize: 20)
        locationLabel.textAlignment = .center
        locationLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.locationLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            self.locationLabel.topAnchor.constraint(equalTo: self.nameLabel.bottomAnchor, constant: 10),
            self.locationLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 5),
            self.locationLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -5),
            self.locationLabel.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    func dateLabelSetting() {
        self.view.addSubview(dateLabel)
        dateLabel.text = "Created at: \(DetailViewController.dateFormating(date: (photo?.created_at)!))"
        dateLabel.textColor = .gray
        dateLabel.font = .systemFont(ofSize: 15)
        dateLabel.textAlignment = .center
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.dateLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            self.dateLabel.topAnchor.constraint(equalTo: self.locationLabel.bottomAnchor, constant: 10),
            self.dateLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 5),
            self.dateLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -5),
            self.dateLabel.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    func addToFavoritesButtonSetting() {
        addToFavoritesButton.role = .primary
        self.view.addSubview(addToFavoritesButton)
        if let data = UserDefaults.standard.value(forKey:"favoritesPhotoData") as? Data {
            let favoritesPhotos = try? PropertyListDecoder().decode(Array<Photo>.self, from: data)
            if favoritesPhotos?.contains(photo!) != true {
                addToFavoritesButton.setTitle("Add to favorites", for: .normal)
                addToFavoritesButton.tintColor = .systemBlue
                addToFavoritesButton.layer.borderColor = UIColor.systemBlue.cgColor
                addToFavoritesButton.addTarget(self, action: #selector(showAlertButtonTappedForAdd(_:)), for: .touchUpInside)
            } else {
                addToFavoritesButton.setTitle("Remove from favorites", for: .normal)
                addToFavoritesButton.tintColor = .red
                addToFavoritesButton.layer.borderColor = UIColor.red.cgColor
                addToFavoritesButton.addTarget(self, action: #selector(showAlertButtonTappedForRemove(_:)), for: .touchUpInside)
            }
        }
        addToFavoritesButton.translatesAutoresizingMaskIntoConstraints = false
        addToFavoritesButton.layer.cornerRadius = 5
        addToFavoritesButton.layer.borderWidth = 1
        NSLayoutConstraint.activate([
            self.addToFavoritesButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            self.addToFavoritesButton.topAnchor.constraint(equalTo: self.dateLabel.bottomAnchor, constant: 10),
            self.addToFavoritesButton.heightAnchor.constraint(equalToConstant: 40),
            self.addToFavoritesButton.widthAnchor.constraint(equalToConstant: 190)
        ])
            
    }
    
    @objc func addToFavorites() {
        if let photo = photo {
            if FavoritesViewController.favoritesPhotosArray.contains(photo) {
                return
            } else {
                FavoritesViewController.favoritesPhotosArray.append(photo)
                FavoritesViewController.saveTofavoritesToUserDefaults()
            }
        }
        self.addToFavoritesButton.setTitle("Remove from favorites", for: .normal)
        self.addToFavoritesButton.tintColor = .red
        self.addToFavoritesButton.layer.borderColor = UIColor.red.cgColor
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
        navigationController?.popViewController(animated: true)
    }
    
    @objc func removeFromFavorites() {
        if let photo = photo {
            let index = FavoritesViewController.favoritesPhotosArray.firstIndex(of: photo)
            if index != nil {
                FavoritesViewController.favoritesPhotosArray.remove(at: index!)
                FavoritesViewController.saveTofavoritesToUserDefaults()
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
                addToFavoritesButton.setTitle("Add to favorites", for: .normal)
                addToFavoritesButton.tintColor = .systemBlue
                addToFavoritesButton.layer.borderColor = UIColor.systemBlue.cgColor
                navigationController?.popViewController(animated: true)
            }
        }
    }
    
    //MARK: - Alert Controller
    @objc func showAlertButtonTappedForAdd(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Adding to favorites", message: "Would you like to add this photo to favorites?", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Continue", style: UIAlertAction.Style.default, handler: { [self] _ in
            addToFavorites()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    @objc func showAlertButtonTappedForRemove(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Removing favorites", message: "Would you like to remove this photo from favorites?", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Continue", style: UIAlertAction.Style.default, handler: { [self] _ in
            removeFromFavorites()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Formating date
    static func dateFormating(date: String) -> String {
        let dateFormatter = DateFormatter()
        let outDateFormatter = DateFormatter()
        outDateFormatter.dateStyle = .medium
        outDateFormatter.timeStyle = .none
        outDateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let dateFromString = dateFormatter.date(from: date)
        guard dateFromString != nil else {
            return "no date"
        }
        return "\(outDateFormatter.string(from: dateFromString!))"
    }
}


