import UIKit

class FavoritesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    static var favoritesPhotosArray: [Photo] = []
    let identifier = FavoriteTableViewCell.identifier
    var favoritesTableView = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FavoritesViewController.favoritesPhotosArray = getFavoritesArray()
        self.navigationItem.title = "Favorites"
        self.view.backgroundColor = .brown
        self.navigationController?.navigationBar.prefersLargeTitles = true
        customTabBarSetting(title: "Favorites", systemImage: "star.fill", tag: 1)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editing(param:)))
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "load"), object: nil)
        createTable()
    }
    
    //MARK: - Custom tabBarItem
    func customTabBarSetting(title: String, systemImage: String, tag: Int) {
        let mainTabBarItem = UITabBarItem(title: title, image: UIImage(systemName: systemImage), tag: tag)
        self.tabBarItem = mainTabBarItem
    }
    //MARK: - Reload data when favorite added
    @objc func loadList(notification: NSNotification){
        self.favoritesTableView.reloadData()
    }
    //MARK: - Create Table
    func createTable() {
        self.favoritesTableView = UITableView(frame: view.bounds, style: .plain)
        self.favoritesTableView.delegate = self
        favoritesTableView.translatesAutoresizingMaskIntoConstraints = false
        favoritesTableView.register(UITableViewCell.self, forCellReuseIdentifier: identifier)
        self.favoritesTableView.dataSource = self
        favoritesTableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        favoritesTableView.register(FavoriteTableViewCell.self, forCellReuseIdentifier: identifier)
        favoritesTableView.separatorStyle = .singleLine
        favoritesTableView.separatorColor = .systemBlue
        view.addSubview(favoritesTableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FavoritesViewController.favoritesPhotosArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! FavoriteTableViewCell
        cell.collectionCellImageView.downloaded(from: (FavoritesViewController.favoritesPhotosArray[indexPath.row].urls?.small!)!)
        cell.collectionCellNameLabel.text = "Author: \(FavoritesViewController.favoritesPhotosArray[indexPath.row].user?.name ?? "no author")"
        let date = "Created at: \(DetailViewController.dateFormating(date: (FavoritesViewController.favoritesPhotosArray[indexPath.row].created_at)!))"
        cell.dateOfCreationLabel.text = "\(date)"
        cell.locationLabel.text = FavoritesViewController.favoritesPhotosArray[indexPath.row].location?.name
        cell.downloadslabel.text = "Downloads: \(FavoritesViewController.favoritesPhotosArray[indexPath.row].downloads ?? 0)"
        return cell
    }
    let tableImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 5
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        return imageView
    }()
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        70.0
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailViewController = DetailViewController()
        guard FavoritesViewController.favoritesPhotosArray.isEmpty != true else { return }
        detailViewController.photo = FavoritesViewController.favoritesPhotosArray[indexPath.row]
        self.navigationController?.pushViewController(detailViewController, animated: true)
        self.favoritesTableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            FavoritesViewController.favoritesPhotosArray.remove(at: indexPath.row)
            favoritesTableView.deleteRows(at: [indexPath], with: .left)
            FavoritesViewController.saveTofavoritesToUserDefaults()
        }
    }
    //MARK: - Editing table
    @objc private func editing(param: UIBarButtonItem) {
        favoritesTableView.setEditing(!favoritesTableView.isEditing, animated: true)
        navigationItem.rightBarButtonItem?.title = favoritesTableView.isEditing ? "Done" : "Edit"
    }
    
    func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell.EditingStyle
    {
        return .delete
    }
    //MARK: - User Defaults
    
    func getFavoritesArray() -> [Photo] {
        var photoFromUserDefaults: [Photo] = []
        if let data = UserDefaults.standard.value(forKey:"favoritesPhotoData") as? Data {
            photoFromUserDefaults = try! PropertyListDecoder().decode(Array<Photo>.self, from: data)
            print(photoFromUserDefaults)
        }
        return photoFromUserDefaults
    }
    
    static func saveTofavoritesToUserDefaults() {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(FavoritesViewController.favoritesPhotosArray), forKey:"favoritesPhotoData")
    }
}
